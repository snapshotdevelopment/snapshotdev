//
//  AlertVC.swift
//  Snapshot
//
//  Created by clyon jackson on 11/13/19.
//  Copyright © 2019 Ours Technologies. All rights reserved.
//

import UIKit

    
func presentAlertViewController(alertString:String, VC:UIViewController){
    let alertController = UIAlertController(title: "✋🏾", message : alertString, preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
    VC.navigationController?.present(alertController, animated: true, completion : nil)
}



