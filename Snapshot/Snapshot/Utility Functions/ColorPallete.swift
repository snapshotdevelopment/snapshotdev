//
//  ColorPallete.swift
//  Snapshot
//
//  Created by clyon jackson on 1/29/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//


import UIKit
public extension UIColor {
    
    class var fiyah: UIColor {
        return UIColor(red:0.98, green:0.09, blue:0.25, alpha:1.0)
    }
    
    class var nipseyBlue: UIColor {
        return  UIColor(red:0.31, green:0.23, blue:1.00, alpha:1.00)
    }
    
    class var jamaicanGold: UIColor {
        return  UIColor(red:1.00, green:0.83, blue:0.11, alpha:1.00)
    }
    
    class var greenMachine: UIColor {
        return  UIColor(red:0.20, green:1.00, blue:0.42, alpha:1.00)
    }
    
    class var snapshotBlue: UIColor {
      return UIColor(red:0.00, green:0.34, blue:1.00, alpha:1.00)
    }
    
    class var snapPurple: UIColor {
      return UIColor(red:0.00, green:0.34, blue:1.00, alpha:1.00)
    }
    
}
