//
//  getSublocality.swift
//  Snapshot
//
//  Created by clyon jackson on 3/23/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import Foundation
import MapKit

enum PlaceMarkError: Error {
    case invalidAddress
}

enum MapkitError: Error {
  case sublocality
}

class MapKitFunctions {
    let validLocalities: Set<String> = ["Queens", "Brooklyn", "Staten Island", "Manhattan", "Bronx"]
    
    func getSubLocality(forAddress address: String, completion: @escaping (Result<String, Swift.Error>) ->()) {
      let geocoder = CLGeocoder()
      geocoder.geocodeAddressString(address) { placemarks, error in
        if let locality = (placemarks ?? []).first(where: { self.validLocalities.contains($0.subLocality ?? "")}) {
          completion(.success(locality.subLocality!))
        } else {
          completion(.failure(MapkitError.sublocality))
        }
      }
    }
}

