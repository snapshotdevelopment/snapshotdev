//
//  ViewController.swift
//  snapAnimation
//
//  Created by clyon jackson on 2/25/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var snapLabel: UILabel!
    @IBOutlet weak var shotLabel: UILabel!
    
    
    @IBOutlet weak var squareHeight: NSLayoutConstraint!
    @IBOutlet weak var shotLeftMargin: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        UIView.animate(withDuration: 0.2, delay: 0.2, options: [], animations: {
            self.shotLabel.transform = CGAffineTransform(translationX: -170, y: 0)
        }) { (finished) in
            
        }
        
        UIView.animate(withDuration: 0.2, delay: 0.4, options: [], animations: {
            self.snapLabel.transform = CGAffineTransform(translationX: -10, y: 0)
            
        }) { (finished) in
            
        }
        
        UIView.animate(withDuration: 0.2, delay: 0.7, options: [], animations: {
            self.snapLabel.transform = CGAffineTransform(translationX: 10, y: 0)
        })
    }
    
    

}

