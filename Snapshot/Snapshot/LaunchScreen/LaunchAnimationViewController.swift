//
//  LaunchAnimationViewController.swift
//  Snapshot
//
//  Created by Jon-Tait Beason on 4/5/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import UIKit

class LaunchAnimationViewController: UIViewController {
  // MARK: - Outlets
  @IBOutlet weak var snapLabel: UILabel!
  @IBOutlet weak var shotLabel: UILabel!
  @IBOutlet weak var animationView: UIView!
  
  private weak var windowDisplayTimer: Timer?
  
  // MARK:- Building Layers
  private var smallBuilding = CALayer()
  private var bigBuilding = CALayer()
  private var allWindows: [BuildingWindow] = []
  
  // Layer to hold the majority of the animation layers
  private var parentLayer = CALayer()
  
  // A transparent layer on which the windows for the big building. This is necessary
  // because it needs to be on top and allow small building windows come through
  private let bigWindowLayer = CALayer()
  
  private let builder = LaunchAnimationBuilder(mainDuration: 1.0)
  
  // MARK: - Constants
  let nameKey = "name"
  let smallBuildingKey = "small-building"
  let largeBuildingKey = "large-building"
  let foundationKey = "foundation"
  
  override func viewDidLoad() {
    super.viewDidLoad()
//    let foundationWidth: CGFloat = 190
//    let frame = animationView.bounds
//
//    // Small building windows
//    let windowBuilder = WindowBuilder(buildingBounds: CGRect(
//      origin: .zero,
//      size: animationView.bounds.size)
//    )
//
//    // Ranges: 4 added to y ranges for offset used in drawing.
//    let largeXRange = Range(uncheckedBounds: (lower: 70, upper: 130))
//    let largeYRange = Range(uncheckedBounds: (lower: 14, upper: 140))
//    let smallXRange = Range(uncheckedBounds: (lower: 20, upper: 70))
//    let smallYRange = Range(uncheckedBounds: (lower: 64, upper: 170))
//
//    // Windows for both buildings
//    allWindows = windowBuilder.buildWindows(
//      small: WindowDimension(xRange: smallXRange, yRange: smallYRange),
//      large: WindowDimension(xRange: largeXRange, yRange: largeYRange)
//    )
//
//    let drawer = BuildingDrawer(
//      lineWidth: 3,
//      frame: frame,
//      buildingColor: UIColor.black.cgColor,
//      windowColor: UIColor.red.cgColor,
//      buildingWidth: 100,
//      buildingHeight: 200
//    )
//    let foundation = drawer.buildFoundation(width: 160, x: 5)
//    smallBuilding = drawer.drawBuilding(type: .small)
//    bigBuilding = drawer.drawBuilding(type: .large)
    
//    // Foundation
//    let foundationAnimation = builder.buildFoundationAnimation(
//      start: 50-foundationWidth,
//      end: 50
//    )
//    foundationAnimation.delegate = self
//    foundationAnimation.setValue(foundationKey, forKey: nameKey)
//
//    parentLayer.masksToBounds = true
//    parentLayer.frame = frame
//
//    // Add building layers
//    parentLayer.addSublayer(smallBuilding)
//    parentLayer.addSublayer(bigBuilding)
////    parentLayer.addSublayer(foundation)
//
//    bigWindowLayer.backgroundColor = UIColor.clear.cgColor
//    bigWindowLayer.frame = frame
//
//    self.animationView.layer.addSublayer(self.bigWindowLayer)
    
    // Animate shot label onto the the screen
    let snapPosition = self.snapLabel.layer.position
    let shotPosition = self.shotLabel.layer.position
    let shotLabelWidth = self.shotLabel.bounds.width
    let distanceToSnapLabel = -distance(snapPosition, shotPosition) + shotLabelWidth + 5
    
    UIView.animate(withDuration: 0.3, delay: 0.5, options: [.curveEaseIn], animations: {
        self.shotLabel.transform = CGAffineTransform(translationX: distanceToSnapLabel, y: 0)
    }){ finished in
        // Animate snap label to the left.
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [], animations: {
            self.snapLabel.transform = CGAffineTransform(translationX: -10, y: 0)
        }) { finished in
            // Animate snap label slightly and start the foundation animtion when it is done.
            UIView.animate(withDuration: 0.2, delay: 0.0, options: [], animations: {
                  self.snapLabel.transform = CGAffineTransform(translationX: 0.009, y: 0)
                }) { finished in
//                  foundation.add(foundationAnimation, forKey: "slide-in")
                    self.animationView.layer.insertSublayer(self.parentLayer, at: 0)
                    self.showSearchController()
                    }
            }
        }
    }
  
  // Hide NavigationController before view appears
  override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      navigationController?.setNavigationBarHidden(true, animated: animated)
  }
  
  // Show navigation controller when view disapperss
  override func viewWillDisappear(_ animated: Bool) {
      super.viewWillDisappear(animated)
      navigationController?.setNavigationBarHidden(false, animated: animated)
  }

  private func startSmallBuildingAnimation() {
    let toValue: CGPoint = .zero
    let fromValue = smallBuilding.position
    let smallBuildingAnimation = builder.getPositionAnimation(toValue: toValue, fromValue: fromValue)
    smallBuildingAnimation.setValue(smallBuildingKey, forKey: nameKey)
    smallBuildingAnimation.delegate = self
    
    smallBuilding.position = .zero // Set final position
    smallBuilding.add(smallBuildingAnimation, forKey: smallBuildingKey) // Override implicit animation
  }
  
  private func startLargeBuildingAnimation() {
    let toValue: CGPoint = .zero
    let fromValue = bigBuilding.position
    let animation = builder.getPositionAnimation(toValue: toValue, fromValue: fromValue)
    animation.delegate = self
    animation.setValue(largeBuildingKey, forKey: nameKey)
    
    bigBuilding.position = .zero // Set final position
    bigBuilding.add(animation, forKey: largeBuildingKey)
  }
  
  /// Add windows to buildings with a `0.05` delay
  private func startWindowAnimation() {
    var time = 0.05
    windowDisplayTimer = Timer.scheduledTimer(
      withTimeInterval: time * Double(allWindows.count) + 0.1,
    repeats: false
    ) { [weak self] timer in
      self?.showSearchController()
    }
    
    for window in allWindows.shuffled() {
      DispatchQueue.main.asyncAfter(deadline: .now() + time) {
        switch window.type {
        case .large:
          self.bigWindowLayer.insertSublayer(window.layer, at: 0)
        case .small:
          self.parentLayer.insertSublayer(window.layer, at: 0)
        }
      }
      time += 0.05
    }
  }
  
  private func showSearchController() {
    windowDisplayTimer?.invalidate()
    
    let storyboard = UIStoryboard(name:"Ticker", bundle: nil)
    let mainViewController =
        storyboard.instantiateViewController(withIdentifier: "Home")
    
    self.dismiss(animated: true) {
        self.navigationController?.pushViewController(mainViewController, animated: false)
    }
    
  }
}

extension LaunchAnimationViewController: CAAnimationDelegate {
  func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
    if let name = anim.value(forKey: nameKey) as? String {
      switch name {
      case foundationKey:
         startSmallBuildingAnimation()
      case smallBuildingKey:
        startLargeBuildingAnimation()
      case largeBuildingKey:
        startWindowAnimation()
      default: break
      }
    }
  }
}

func distance(_ a: CGPoint, _ b: CGPoint) -> CGFloat {
    let xDist = a.x - b.x
    let yDist = a.y - b.y
    return CGFloat(sqrt(xDist * xDist + yDist * yDist))
}


