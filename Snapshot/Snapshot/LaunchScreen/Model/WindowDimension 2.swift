//
//  WindowDimension.swift
//  Snapshot
//
//  Created by Jon-Tait Beason on 4/5/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import Foundation

// Dimension, including origin of window given as ranges.
// xRange: 5...10 means the window as a width of 5
struct WindowDimension  {
  let xRange: Range<Int>
  let yRange: Range<Int>
}
