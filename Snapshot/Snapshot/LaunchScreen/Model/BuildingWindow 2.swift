//
//  BuildingWindow.swift
//  Snapshot
//
//  Created by Jon-Tait Beason on 4/5/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import UIKit

final class BuildingWindow {
  let layer: CALayer
  let type: BuildingArt
  
  init(layer: CALayer, type: BuildingArt) {
    self.layer = layer
    self.type = type
  }
}
