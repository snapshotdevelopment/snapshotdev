//
//  LaunchAnimationBuilder.swift
//  Snapshot
//
//  Created by Jon-Tait Beason on 4/5/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import UIKit

enum AnimationKeyPath: String {
  case rotation, translation, position, transform
}

final class LaunchAnimationBuilder {
  let mainDuration: Double
  
  init(mainDuration: Double) {
    self.mainDuration = mainDuration
  }
  
  func buildFoundationAnimation(start: CGFloat, end: CGFloat) -> CABasicAnimation {
    let animation = CABasicAnimation(
      keyPath: "transform.\(AnimationKeyPath.translation.rawValue).x"
    )
    animation.timingFunction = CAMediaTimingFunction(name: .easeIn)
    animation.duration = mainDuration / 2
    animation.fromValue = start
    animation.toValue = end
    
    return animation
  }
  
  func getPositionAnimation(toValue: CGPoint, fromValue: CGPoint) -> CABasicAnimation {
    let animation = CABasicAnimation(keyPath: "position")
    animation.duration = 0.5
    animation.toValue = toValue
    animation.fromValue = fromValue
    animation.fillMode = .forwards
    
    return animation
  }
}
