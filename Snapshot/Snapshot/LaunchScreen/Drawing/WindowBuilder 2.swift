//
//  WindowBuilder.swift
//  Snapshot
//
//  Created by Jon-Tait Beason on 4/5/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import UIKit

final class WindowBuilder {
  let buildingBounds: CGRect
  init(buildingBounds: CGRect) {
    self.buildingBounds = buildingBounds
  }
  
  func buildWindows(small: WindowDimension, large: WindowDimension) -> [BuildingWindow] {
    var windows: [BuildingWindow] = []
    windows.append(contentsOf: buildWindows(dimension: small, type: .small))
    windows.append(contentsOf: buildWindows(dimension: large, type: .large))
    
    return windows
  }
  
  func buildWindows(dimension: WindowDimension, type: BuildingArt) -> [BuildingWindow] {
     var windows: [BuildingWindow] = []
     let size = CGSize(width: 20, height: 20)
     
    for y in stride(from: dimension.yRange.lowerBound, through: dimension.yRange.upperBound, by: 30) {
      for x in stride(from: dimension.xRange.lowerBound, through: dimension.xRange.upperBound, by: 30) {
         let path = UIBezierPath(rect: CGRect(origin: CGPoint(x: x, y: y), size: size))
         let shapeLayer = CAShapeLayer()
         shapeLayer.frame = buildingBounds
         shapeLayer.path = path.cgPath

         windows.append(BuildingWindow(layer: shapeLayer, type: type))
       }
     }
     return windows
   }
}
