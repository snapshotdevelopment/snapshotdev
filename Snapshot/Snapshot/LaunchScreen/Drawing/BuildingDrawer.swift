//
//  BuildingDrawer.swift
//  Snapshot
//
//  Created by Jon-Tait Beason on 4/5/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import UIKit

enum BuildingArt {
  case large, small
}

final class BuildingDrawer {
  private let lineWidth: CGFloat
  private let buildingColor: CGColor
  private let windowColor: CGColor
  private let buildingHeight: CGFloat
  private let buildingWidth: CGFloat
  private let frame: CGRect
  
  init(lineWidth: CGFloat, frame: CGRect, buildingColor: CGColor, windowColor: CGColor, buildingWidth: CGFloat, buildingHeight: CGFloat) {
    self.buildingColor = buildingColor
    self.windowColor = windowColor
    self.lineWidth = lineWidth
    self.frame = frame
    self.buildingWidth = buildingWidth
    self.buildingHeight = buildingHeight
  }
  
  
  /// Draw foundation for the building. A thin line below the rectangles
  /// - Parameters:
  ///   - width: Width of foundation
  ///   - x: Starting `x`
  func buildFoundation(width: CGFloat, x: CGFloat) -> CAShapeLayer {
    let path = UIBezierPath()
    
    let y = frame.height-(lineWidth/2)
    path.move(to: CGPoint(x: x, y: y))
    path.addLine(to: CGPoint(x: x+width, y: y))
    
    let foundation = buildOutline(path: path, color: buildingColor)
    foundation.lineCap = .round
    
    return foundation
  }
  
  /// Draw building
  /// - Parameter type: The type of rect for each building
  func drawBuilding(type: BuildingArt) -> CALayer {
    // Default to large building
    var startX: CGFloat = 60
    var height = buildingHeight
    
    // Adjust if building is small
    if type == .small {
      startX = 10
      height = buildingHeight*0.75
    }
    
    let layer = drawBuilding(height: height, startX: startX, type: type)
    
    // Large building must be filled to hide small building behind it
    if type == .large {
      layer.fillColor = UIColor.white.cgColor
    }
    
    // Parent Layer
    let parentLayer = CALayer()
    parentLayer.anchorPoint = .zero
    parentLayer.bounds = CGRect(origin: .zero, size: CGSize(width: frame.width, height: frame.height))
    parentLayer.position = CGPoint(x: 0, y: frame.height)

    // Add building to parent
    parentLayer.addSublayer(layer)

    return parentLayer
  }
  
  /// Build outline for building using bezierpaths. The final path is used as the `path` for the shape representing the building
  /// - Parameters:
  ///   - height: Height of the building
  ///   - startX: Start x
  ///   - type: Type of building
  private func drawBuilding(height: CGFloat, startX: CGFloat = .zero, type: BuildingArt) -> CAShapeLayer {
    let offset: CGFloat = 4
    let origin = CGPoint(x: startX, y: frame.height-height+lineWidth+offset)
    let rect = CGRect(origin: origin, size: CGSize(width: buildingWidth, height: height + offset))
    
    let path = UIBezierPath(roundedRect: rect, cornerRadius: 2.0)
    
    // Add door to large building
    if type == .large {
      let x = startX+33
      let y = frame.height-33-lineWidth+offset
      let doorRect = CGRect(x: x, y: y, width: 30, height: 33+offset)
      let door = UIBezierPath(roundedRect: doorRect, cornerRadius: 2.0)
      
      path.append(door)
    }
    
    let shapeLayer = buildOutline(path: path, color: buildingColor)
    
    // It doesn't have a superlayer and is dimensionless so don't set the frame.
    // https://stackoverflow.com/questions/18955210/frame-and-bounds-of-cashapelayer
    // shapeLayer.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)

    return shapeLayer
  }
  
  /// Build a no fill shape layer with a bezierpath
  /// - Parameters:
  ///   - path: Bezier path
  private func buildOutline(path: UIBezierPath, color: CGColor, anchor: CGPoint = .zero) -> CAShapeLayer {
    let layer = CAShapeLayer()
    layer.lineWidth = lineWidth
    layer.fillColor = nil
    layer.strokeColor = color
    layer.path = path.cgPath
    
    return layer
  }
}
