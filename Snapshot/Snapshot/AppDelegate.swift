//
//  AppDelegate.swift
//  Snapshot
//
//  Created by clyon jackson on 9/16/19.
//  Copyright © 2019 Ours Technologies. All rights reserved.
//

import UIKit
import Starscream
import GoogleMaps
import CoreData
import Firebase

let cache = NSCache<NSString, PropertyCharacteristics>()
let db = Firestore.firestore()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var searchHistory = [String]()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions
                    launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        GMSServices.provideAPIKey("AIzaSyDBuVJzQkEOexwc-CADKuaSTxpfAA8Nu9I")
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        SearchHistory().create()
        return true
    }
}
