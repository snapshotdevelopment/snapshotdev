//
//  PropertyDetailsCollectionViewController.swift
//  Snapshot
//
//  Created by clyon jackson on 11/21/19.
//  Copyright © 2019 Ours Technologies. All rights reserved.
//

import UIKit
import ViewAnimator
import VegaScrollFlowLayout

class PropertyDetailsCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    //Variables
    var propertyDetails = [PropertyDescriptor]()
    var shouldAnimateCells = false
    var searchBarView : UIView?
    var mapView : UIView?
    private var lastContentOffset: CGFloat = 0
    
    @IBOutlet weak var layout: UICollectionViewFlowLayout!
    
    @IBOutlet weak var pvCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//
        let layout = VegaScrollFlowLayout()
        self.pvCollectionView.collectionViewLayout = layout
        layout.minimumLineSpacing = 20
        layout.itemSize = CGSize(width: self.pvCollectionView.frame.width, height: 87)
        layout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        self.view.layer.cornerRadius = 20
    }

    //Protocols
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.propertyDetails.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pdCell", for: indexPath as IndexPath) as! PropertyDetailsCollectionViewCell
        
         cell.layer.cornerRadius = 10
         cell.label.text = String(self.propertyDetails[indexPath.row].descriptor.0)
         cell.value.text = String(self.propertyDetails[indexPath.row].descriptor.1)
         
         let imageView = UIImageView(frame: CGRect(x: 100, y: 150, width: 10, height: 10))
         let icon = UIImage(named: "map")
         imageView.image = icon
         cell.icon.addSubview(imageView)
         cell.icon.image = icon
         
         if shouldAnimateCells == true {
             let fromAnimation = AnimationType.from(direction: .right, offset: 1300.0)
             UIView.animate(views: collectionView.subviews, animations: [fromAnimation])
             self.shouldAnimateCells = false
         }
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // 1
        return 1
    }
}


