//
//  PropertyDetailsCollectionViewController.swift
//  Snapshot
//
//  Created by clyon jackson on 11/21/19.
//  Copyright © 2019 Ours Technologies. All rights reserved.
//

import UIKit
import ViewAnimator


class PropertyDetailsCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    //Variables
    var propertyDetails = [PropertyDescriptor]()
    var shouldAnimateCells = false
    var searchBarView : UIView?
    var mapView : UIView?
    var zone3Container : UIView?

    private var lastContentOffset: CGFloat = 0
    // 
    @IBOutlet weak var collectionViewLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var pvCollectionView: UICollectionView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.layer.cornerRadius = 10
        self.view.frame = (CGRect(x: 0, y: 0, width:self.view.frame.width, height: (self.zone3Container?.frame.height)!))
        self.pvCollectionView.delegate = self
        self.pvCollectionView.dataSource = self
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    //Protocols
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.propertyDetails.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pdCell", for: indexPath as IndexPath) as! PropertyDetailsCollectionViewCell
        
        cell.layer.cornerRadius = 4
        cell.clipsToBounds = true
        
        // Set Label
        cell.label.text = String(self.propertyDetails[indexPath.row].descriptor.labelName)
        cell.value.text = String(self.propertyDetails[indexPath.row].descriptor.value)
        return cell
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // 1
        return 1
    }

}


