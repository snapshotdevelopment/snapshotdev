//
//  DOBPropertyData.swift
//  Snapshot
//
//  Created by clyon jackson on 9/22/19.
//  Copyright © 2019 Ours Technologies. All rights reserved.
//

import UIKit

struct PropertyCharacteristics {
    let zone : String
    let accrisURL: String
    let dobURL : String
    let marketValue : String
    let owner : String
    let yearBuilt : String
    let units : String
    let lotFront : String
    let lotDepth : String
    let landArea : String
    let buildingClass : String
    let buildingCount : String
    let stories : String
    let buildingDimensionFront : String
    let buildingDemtionDepth : String
    let buildingSQFT : String
    let boro : String
    let block : String
    let lot : String
    let fullAddress : String
    let neighborhood : String
    let lattitude : Double
    let longitude : Double
    
    init(zone:String, accrisURL:String, dobURL:String, marketValue:String, owner:String, yearBuilt:String, units:String, lotFront:String, lotDepth:String, landArea:String, buidlingClass:String, buildingCount:String, stories:String, buildingDimensionFront:String, buildingDimensionDepth:String, buildingSQFT:String, boro:String, block:String, lot:String, fullAddress:String, neighborhood:String, lattitude:Double, longitude:Double){
        
        self.zone = zone
        self.accrisURL = accrisURL
        self.dobURL = dobURL
        self.marketValue = marketValue
        self.owner = owner
        self.yearBuilt = yearBuilt
        self.units = units
        self.lotFront = lotFront
        self.lotDepth = lotDepth
        self.landArea = landArea
        self.buildingClass = buidlingClass
        self.buildingCount = buildingCount
        self.stories = stories
        self.buildingDimensionFront = buildingDimensionFront
        self.buildingDemtionDepth = buildingDimensionDepth
        self.buildingSQFT = buildingSQFT
        self.boro = boro
        self.block = block
        self.lot = lot
        self.fullAddress = fullAddress
        self.neighborhood = neighborhood
        self.lattitude = Double(lattitude)
        self.longitude = longitude
    }

    func propertyDetailsDescriptors() -> [PropertyDescriptor] {
        let descriptors = [
            PropertyDescriptor(descriptor: (labelName: "Owner", value: owner, category:"All", icon: "owner" )),
            PropertyDescriptor(descriptor: (labelName: "Units" , value: units, category:"All", icon: "units")),
            PropertyDescriptor(descriptor: (labelName: "Zone", value: zone, category:"All", icon: "Zone")),
            PropertyDescriptor(descriptor: (labelName: "Building Class", value: buildingClass, category:"All", icon: "Building Class")),
            PropertyDescriptor(descriptor: (labelName: "Lot Width", value: lotFront, category:"All", icon: "Lot Width")),
            PropertyDescriptor(descriptor: (labelName: "Lot Depth", value: lotDepth, category:"All", icon: "Lot Depth")),
            PropertyDescriptor(descriptor: (labelName: "Land Area", value: landArea, category:"All", icon: "lotArea")),
            PropertyDescriptor(descriptor: (labelName: "Building Front", value: buildingDimensionFront, category:"All", icon: "Building Front")),
            PropertyDescriptor(descriptor: (labelName: "Building Depth", value: buildingDemtionDepth, category:"All", icon: "Building Depth")),
            PropertyDescriptor(descriptor: (labelName: "Building Count", value: buildingCount, category:"All", icon: "Building Count")),
            PropertyDescriptor(descriptor: (labelName: "Building SQFT" , value: buildingSQFT, category:"All", icon: "Building SQFT")),
            PropertyDescriptor(descriptor: (labelName: "Stories", value: stories, category:"All", icon: "Storie")),
            PropertyDescriptor(descriptor: (labelName: "YearBuilt", value: yearBuilt, category:"All", icon: "calendar")),
            PropertyDescriptor(descriptor: (labelName: "Market Value", value: marketValue, category:"All", icon: "marketValue"))
        ]
        return descriptors
    }
    
}
extension PropertyCharacteristics:Codable {
    enum PropertyCharacteristicsKeys: String, CodingKey { // declarekeys
      case zone = "zone"
      case id = "id"
      case twitter = "twitter"
      case accrisURL = "accris_link"
      case dobURL = "dob_link"
      case marketValue = "market_value"
      case owner = "owner"
      case yearBuilt = "year_built"
      case units = "units"
      case lotFront = "lot_front"
      case lotDepth = "lot_depth"
      case landArea = "land_area"
      case buidlingClass = "buidling_class"
      case buildingCount = "number_of_buildings"
      case stories = "stories"
      case buildingFront = "building_front"
      case buildingDepth = "building_depth"
      case buildingSQFT = "building_sqft"
      case boro = "boro"
      case block = "block"
      case lot = "lot"
      case fullAddress = "full_address"
      case neighborhood = "neighborhood"
      case lattitude = "lattitude"
      case longitude = "longitude"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: PropertyCharacteristicsKeys.self)
        let zone = try container.decode(String.self, forKey: .zone)
        let accrisURL = try container.decode(String.self, forKey: .accrisURL)
        let dobURL = try container.decode(String.self, forKey: .dobURL)
        let marketValue = try container.decode(String.self, forKey: .marketValue)
        let owner = try container.decode(String.self, forKey: .owner)
        let yearBuilt = try container.decode(String.self, forKey: .yearBuilt)
        let units = try container.decode(String.self, forKey: .units)
        let lotFront = try container.decode(String.self, forKey: .lotFront)
        let lotDepth = try container.decode(String.self, forKey: .lotDepth)
        let landArea = try container.decode(String.self, forKey: .landArea)
        let buidlingClass = try container.decode(String.self, forKey: .buidlingClass)
        let buildingCount = try container.decode(String.self, forKey: .buildingCount)
        let stories = try container.decode(String.self, forKey: .stories)
        let buildingFront = try container.decode(String.self, forKey: .buildingFront)
        let buildingDepth = try container.decode(String.self, forKey: .buildingDepth)
        let buildingSQFT = try container.decode(String.self, forKey: .buildingSQFT)
        let boro = try container.decode(String.self, forKey: .boro)
        let block = try container.decode(String.self, forKey: .block)
        let lot = try container.decode(String.self, forKey: .lot)
        let fullAddress = try container.decode(String.self, forKey: .fullAddress)
        let neighborhood = try container.decode(String.self, forKey: .neighborhood)
        let lattitude = try container.decode(Float.self, forKey: .lattitude)
        let longitude = try container.decode(Float.self, forKey: .longitude)
        
        self.init (zone:zone, accrisURL:accrisURL, dobURL:dobURL, marketValue:marketValue,
                   owner:owner,yearBuilt:yearBuilt, units:units, lotFront:lotFront,
                   lotDepth:lotDepth, landArea:landArea, buidlingClass:buidlingClass,
                   buildingCount:buildingCount, stories:stories, buildingDimensionFront:buildingFront,
                   buildingDimensionDepth:buildingDepth, buildingSQFT:buildingSQFT, boro:boro,
                   block:block, lot:lot, fullAddress:fullAddress,neighborhood:neighborhood,
                   lattitude:Double(lattitude), longitude:Double(longitude))
    }
}

struct PropertyDescriptor {
    let descriptor : (labelName: String, value: String, category: String, icon: String)
}


