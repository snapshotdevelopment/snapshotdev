//
//  fetchPropertyDetails.swift
//  Snapshot
//
//  Created by clyon jackson on 11/19/19.
//  Copyright © 2019 Ours Technologies. All rights reserved.
//

import Foundation
import UIKit
import Alamofire


func getPropertyCharacteristics(address:String, searchViewController:SearchParentViewController, completion: @escaping(PropertyCharacteristics) -> Void) {
    let parameters: Parameters = ["address":address]
    let endpoint = "https://mqrj7gdv87.execute-api.us-east-1.amazonaws.com/dev/PropertDetails"
    let request = AF.request(endpoint, method: .get, parameters:parameters)
    
    request.responseJSON { response in
        guard let data = response.data else{return}
        do {
            completion(try JSONDecoder().decode(PropertyCharacteristics.self, from: data))
            
        } catch {
            if (error.localizedDescription == "The data couldn’t be read because it is missing.") {
                    presentAlertViewController(alertString: "Invalid Address", VC: searchViewController)
                searchViewController.removeLoadingIndicatorViewController(content: searchViewController.loadingIndicatorViewController)
            }
        }
    }
}
