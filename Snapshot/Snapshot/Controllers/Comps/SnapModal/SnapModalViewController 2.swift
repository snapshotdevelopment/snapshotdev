//
//  SnapModalViewController.swift
//  Snapshot
//
//  Created by clyon jackson on 1/22/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import UIKit
import MapKit

class SnapModalViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var buildingClassLabel: UILabel!
    @IBOutlet weak var saleDate: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet var blurredBackdrop: UIView!
    @IBOutlet weak var modalBackgroundView: UIView!
    
    @IBAction func touchedAddToCompButton(_ sender: Any) {
        
    }
    
    
    // Mark Actions
    @IBAction func closeModal(_ sender: Any) {
        self.dismiss(animated: true) {
            return
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.modalBackgroundView.layer.cornerRadius = 20
            
        }
    }
    


}
