//
//  CompCollectionViewCell.swift
//  Snapshot
//
//  Created by clyon jackson on 2/3/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import UIKit

class CompCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var salePriceLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var saleDateLabel: UILabel!
    @IBOutlet weak var lengthOfOwnerShipLabel: UILabel!
    @IBOutlet weak var cellView: UIView!
    
    @IBOutlet weak var buildingSqftLabel: UILabel!
    @IBOutlet weak var buildingSqftSymbol: UIImageView!
    
    @IBOutlet weak var lotSymbol: UIImageView!
    @IBOutlet weak var lotSqftLabel: UILabel!
    
    @IBOutlet weak var plusMinusSymbol: UIImageView!
    @IBOutlet weak var plusMinusLabel: UILabel!
    
    @IBOutlet weak var distanceSymbol: UIImageView!
    @IBOutlet weak var checkmark: UIImageView!
}
