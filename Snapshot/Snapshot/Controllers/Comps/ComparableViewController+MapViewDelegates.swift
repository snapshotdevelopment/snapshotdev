//
//  ComparableViewController+MapViewDelegates.swift
//  Snapshot
//
//  Created by clyon jackson on 2/10/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class Arc : MKOverlayPathRenderer {
    let arcCenterLattitude = CGFloat()
    let arcCenterYLongitude = CGFloat()
    var length = CGFloat()
    var compViewController = ComparableViewController()
    
    override func createPath() {
        let radius : CGFloat = 1000.0
        let startAngle: CGFloat = CGFloat(180) * .pi / 180
        let endAngle: CGFloat = 0 * .pi / 180
        
        let shapeLayer = CAShapeLayer()
        let path = UIBezierPath(arcCenter: CGPoint(x: 0, y: 0), radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        shapeLayer.path = path.cgPath
        
        

        let view = UIView()
        view.layer.addSublayer(shapeLayer)
        let layer = view.layer
        
        DispatchQueue.main.async {
            self.compViewController.shapeView.addSubview(view)
            var rotationAndPerspectiveTransform = CATransform3DIdentity
            rotationAndPerspectiveTransform.m34 = 1.0 / -260
            rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, 45.0 * .pi / 180.0, 0.0, 6.0, 0.0)
            layer.transform = rotationAndPerspectiveTransform

        }
        self.path = path.cgPath
    }
}
// MARK: - Mapkit Delegates
extension ComparableViewController {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay.isKind(of: MKPolyline.self)  {
            
            // draw the track
            let polyLine = overlay
            let arc = Arc()
            
            arc.length = CGFloat(polyLine.boundingMapRect.width)
            arc.compViewController = self
            
            let polyLineRenderer = Arc(overlay: polyLine)
            
            polyLineRenderer.strokeColor = .fiyah
            polyLineRenderer.lineWidth = 2.0
            polyLineRenderer.lineCap = .round
            return polyLineRenderer
        }
        return MKPolylineRenderer()
    }
    
    
      
}
