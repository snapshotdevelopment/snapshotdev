//
//  ComarablesTableViewCell.swift
//  Snapshot
//
//  Created by clyon jackson on 1/21/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import UIKit

class ComparablesTableViewCell: UITableViewCell {

    
    @IBOutlet weak var saleDate: UILabel?
    @IBOutlet weak var salePrice: UILabel?
    @IBOutlet weak var addressLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.accessibilityLabel = .none
    }
}
