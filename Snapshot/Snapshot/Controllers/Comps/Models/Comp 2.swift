//
//  Comps.swift
//  Snapshot
//
//  Created by clyon jackson on 1/9/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import Foundation

struct Comp:Decodable, Hashable{
    let address : String
    let sale_price : String
    let sale_date : String
    let building_class : String
    let land_sqft : String
    let residential_units : String
    
    
    var hashValues: String {
        get {
            return address + sale_date + sale_price
        }
    }
    static func == (lhs: Comp, rhs: Comp) -> Bool {
        return lhs.address == rhs.address
    }
}
