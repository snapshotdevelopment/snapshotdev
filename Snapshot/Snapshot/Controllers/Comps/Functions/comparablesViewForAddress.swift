//
//  comparablesViewForAddress.swift
//  Snapshot
//
//  Created by clyon jackson on 1/21/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import UIKit

func comparablesForAddress(address:String, comparables:[Comp]) -> ComparableViewController {
    let storyboard = UIStoryboard(name: "ComparablesViewController", bundle: nil)
    let comparableViewController =
        storyboard.instantiateViewController(withIdentifier: "ComparablesViewController") as! ComparableViewController
    comparableViewController.generatedComps = comparables
    comparableViewController.loadView()
    return comparableViewController
    
}
