//
//  averageSalesOfSelectedComparables.swift
//  Snapshot
//
//  Created by clyon jackson on 1/17/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import Foundation
import UIKit

func averageSalesOfSelectedComps(comparables:Set<Comp>) -> String {
    var sales = [Int]()
    for comp in comparables{
        let salesPrice = comp.sale_price.replacingOccurrences(of: ",", with: "")
        let salesPriceInt = Int(salesPrice) ?? 0
        if salesPriceInt != 0 {
            sales.append(salesPriceInt)
        }
    }
    
    func average(numbers: [Int]) -> String {
        var sum = 0
        for number in numbers {
            sum += number
        }
        var  ave : Double = Double(sum) / Double(numbers.count)
        let formatter = NumberFormatter()
        formatter.currencySymbol = "$"
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = .currency
        
        return formatter.string(from: NSNumber(value: ave)) ?? "None"
    }
    return average(numbers: sales)
}
