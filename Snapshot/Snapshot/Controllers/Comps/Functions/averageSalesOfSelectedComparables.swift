//
//  averageSalesOfSelectedComparables.swift
//  Snapshot
//
//  Created by clyon jackson on 1/17/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import Foundation
import UIKit

var sales = [Int]()

func averageSalesOfSelectedComps(comparables:Set<Comp>) -> Double {
    
    for comp in comparables {
        let salesPrice = comp.sale_price.replacingOccurrences(of: ",", with: "")
        let salesPriceInt = Int(salesPrice) ?? 0
        sales.append(salesPriceInt)
    }
    
    func average(numbers: [Int]) -> Double {
        var sum = 0
        for number in numbers {
            sum += number
        }
        
        let  average : Double = Double(sum) / Double(numbers.count)
        sales = [Int]()
        return average
    }
    return average(numbers: sales)
}
