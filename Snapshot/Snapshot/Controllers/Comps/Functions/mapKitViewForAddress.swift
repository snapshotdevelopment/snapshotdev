//
//  mapKitViewForAddress.swift
//  Snapshot
//
//  Created by clyon jackson on 1/23/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import MapKit

func mapKitViewForAddress(address:String, mapView:MKMapView) {
    
    let location = address
    let geocoder:CLGeocoder = CLGeocoder()
    
    geocoder.geocodeAddressString(location) { placemarks, error in
        if let placemark = placemarks?.first {
            let location = placemark.location
            let mark = MKPlacemark(placemark: placemark)
            DispatchQueue.main.async {
                
                let distance: CLLocationDistance = 650
                
                let pitch: CGFloat = 0
                let heading = 180.0
                let camera = MKMapCamera(lookingAtCenter: mark.coordinate,
                                         fromDistance: distance,
                                         pitch: pitch,
                                         heading: heading)
                
                mapView.region.center = location!.coordinate
                mapView.region.span.longitudeDelta /= 8.0
                mapView.region.span.latitudeDelta /= 8.0
                mapView.addAnnotation(mark)
                mapView.cameraZoomRange = MKMapView.CameraZoomRange(minCenterCoordinateDistance:100, maxCenterCoordinateDistance: 1400000)
                mapView.setCamera(camera, animated:false)
                
            }
        }
    }
}

func coordinates(forAddress address: String, completion: @escaping (CLLocationCoordinate2D?) -> Void) {
    let geocoder = CLGeocoder()
    geocoder.geocodeAddressString(address) {
        (placemarks, error) in
        guard error == nil else {
            print("Geocoding error: \(error!)")
            completion(nil)
            return
        }
        completion(placemarks?.first?.location?.coordinate)
    }
}

func addAnnotation(viewcontroller:ComparableViewController, comp: Comp, mapView:MKMapView) {
    let geocoder:CLGeocoder = CLGeocoder();
    geocoder.geocodeAddressString(comp.address) { placemarks, error in
        if let placemark = placemarks?.first {
            let mark = MKPlacemark(placemark: placemark)
            let location = placemark.location
            viewcontroller.mapAnnotations[comp] = mark
            DispatchQueue.main.async {
                mapView.addAnnotation(mark)
                
                mapView.cameraZoomRange = MKMapView.CameraZoomRange(minCenterCoordinateDistance:100, maxCenterCoordinateDistance: 140000)
            }
        }
    }
}

func removeAnnotation(viewController:ComparableViewController, mapView:MKMapView, comp:Comp) {
    guard let annottaion = viewController.mapAnnotations[comp] else { return }
    mapView.removeAnnotation(annottaion)
    
}

