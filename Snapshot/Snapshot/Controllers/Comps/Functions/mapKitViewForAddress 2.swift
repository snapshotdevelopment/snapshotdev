//
//  mapKitViewForAddress.swift
//  Snapshot
//
//  Created by clyon jackson on 1/23/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import MapKit

func mapKitViewForAddress(address:String, mapView:MKMapView) {
    let location = address;
    let geocoder:CLGeocoder = CLGeocoder();
    
    geocoder.geocodeAddressString(location) { placemarks, error in
        if let placemark = placemarks?.first {
            let location = placemark.location
            let mark = MKPlacemark(placemark: placemark)
            mapView.region.center = location!.coordinate
            mapView.region.span.longitudeDelta /= 8.0
            mapView.region.span.latitudeDelta /= 8.0
//            mapView.setRegion(mapView.region, animated: true)
            mapView.addAnnotation(mark)
            mapView.cameraZoomRange = MKMapView.CameraZoomRange(maxCenterCoordinateDistance:400)
            mapView.layer.cornerRadius = 10
        }
    }
}

func updateComparableMapView(viewcontroller:ComparableViewController, comp: Comp, mapView:MKMapView) {
    let geocoder:CLGeocoder = CLGeocoder();
    
    geocoder.geocodeAddressString(comp.address) { placemarks, error in
        if let placemark = placemarks?.first {
            let mark = MKPlacemark(placemark: placemark)
            let location = placemark.location
            mapView.region.center = location!.coordinate
            mapView.region.span.longitudeDelta /= 8.0
            mapView.region.span.latitudeDelta /= 8.0
            mapView.setRegion(mapView.region, animated: true)
            
            viewcontroller.mapAnnotations[comp] = mark
            mapView.setCenter(placemark.location!.coordinate, animated: true)
            DispatchQueue.main.async {
               mapView.addAnnotation(mark)
               mapView.cameraZoomRange = MKMapView.CameraZoomRange(minCenterCoordinateDistance:100, maxCenterCoordinateDistance: 14000)
            }
        }
    }
}

func removeAnnotation(viewController:ComparableViewController, mapView:MKMapView, comp:Comp){
    guard let annottaion = viewController.mapAnnotations[comp] else { return }
    mapView.removeAnnotation(annottaion)
    
}
