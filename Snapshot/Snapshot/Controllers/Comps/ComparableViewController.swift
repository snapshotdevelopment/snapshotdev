//
//  ComparableTableViewController.swift
//  Snapshot
//
//  Created by clyon jackson on 1/21/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import UIKit
import MapKit

class ComparableViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, MKMapViewDelegate {
    // MARK: - Properties
    var shapeView = UIView()
    let boroughCodes = ["1": "Manhattan", "2":"Bronx", "3":"Brooklyn", "4":"Queens", "5":"Staten Island"]
    var selectedComparables = Set<Comp>()
    var generatedComps = [Comp]()
    var compAnchor = PropertyCharacteristics(zone: "", accrisURL: "", dobURL: "", marketValue: "", owner: "",
                                             yearBuilt: "", units: "", lotFront: "", lotDepth: "", landArea: "",
                                             buidlingClass: "", buildingCount: "", stories: "", buildingDimensionFront: "",
                                             buildingDimensionDepth: "", buildingSQFT: "", boro: "", block: "", lot: "",
                                             fullAddress: "", neighborhood: "", lattitude: 0.0, longitude: 0.0, comps: [])
    
    var selectedComp = Comp(address: "", sale_price: "", sale_date: "", building_class: "", land_sqft: "", residential_units: "", borough: "", neighborhood: "", gross_square_feet: "", year_built: "")
    var mapAnnotations = [Comp: MKPlacemark]()
    var didAnimateMap = Bool()
    var overlays = [MKOverlay]()
    
    // MARK: - Outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var selectedCompsAverageLabel: UILabel!
    
    // MARK: - Collection View
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Actions
    @IBAction func didPressClear(_ sender: Any) {
        for annotation in self.mapView.annotations {
            mapView.removeAnnotation(annotation)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        mapKitViewForAddress(address: self.compAnchor.fullAddress, mapView: self.mapView)
        
    }

    // MARK: - CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.generatedComps.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let comp = self.generatedComps[indexPath.row]
        let nib = UINib(nibName: "compCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "compCell")
        
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "compCell", for: indexPath) as! CompCollectionViewCell
        customizeCompCell(cell: cell)
        
        // Make UI changes on background thread
        DispatchQueue.main.async {
            if self.selectedComparables.contains(comp) == true {
                cell.backgroundColor = .fiyah
            } else {
                cell.backgroundColor = nil
            }
            cell.addressLabel.text = comp.address
            cell.saleDateLabel.text = comp.sale_date
            cell.buildingSqftLabel.text = comp.land_sqft
            cell.salePriceLabel.text = comp.sale_price
            cell.layer.cornerRadius = 10
            cell.buildingSqftLabel.text = comp.gross_square_feet
            
            // Land SQFT Symbol
            let landSqftSymbol = UIImage(systemName: "rectangle.3.offgrid")?.withTintColor(.darkGray)
            cell.lotSymbol.image = landSqftSymbol
            cell.lotSqftLabel.text = comp.land_sqft
            
            // Building SQFT Symbol
            let buildingSqftSymbol = UIImage(systemName: "house")
            cell.buildingSqftSymbol.image = buildingSqftSymbol
            cell.buildingSqftLabel.text = comp.gross_square_feet
            
            //Distance Symbol
            let distanceSymbol = UIImage(systemName: "mappin.circle.fill")
            cell.distanceSymbol.image = distanceSymbol
            
            //Distance Symbol
            let plusMinusSymbol = UIImage(systemName: "plus.slash.minus")
            cell.plusMinusSymbol.image = plusMinusSymbol
        }
        
        let longPress = UILongPressGestureRecognizer.init(target: self, action: #selector(self.handleLongPress(gesture:)))
        longPress.minimumPressDuration = 0.25
        cell.addGestureRecognizer(longPress)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "compCell", for: indexPath) as? CompCollectionViewCell{
            cell.cellView.removeFromSuperview()
            NSLayoutConstraint.deactivate([
                cell.cellView.subviews[0].heightAnchor.constraint(equalTo: cell.cellView.heightAnchor),
                cell.cellView.subviews[0].widthAnchor.constraint(equalTo: cell.cellView.widthAnchor)])
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) {
            let comp = generatedComps[indexPath.row]
           DispatchQueue.main.async {
               cell.backgroundColor = .fiyah
               self.selectedComparables.insert(comp)
               self.selectedCompsAverageLabel.text = averageSalesOfSelectedComps(comparables: self.selectedComparables).description
           }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        let comp = generatedComps[indexPath.row]
        
        DispatchQueue.main.async {
            cell?.backgroundColor = nil
            self.selectedComparables.remove(comp)
            self.selectedCompsAverageLabel.text = averageSalesOfSelectedComps(comparables: self.selectedComparables).description
        }
    }
}

extension ComparableViewController {
    private func customizeCompCell(cell: CompCollectionViewCell) {
        cell.cellView.backgroundColor = .clear
        let blurEffect = UIBlurEffect(style: .light)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.translatesAutoresizingMaskIntoConstraints = false
        cell.cellView.insertSubview(blurView, at: 0)
        NSLayoutConstraint.activate([ blurView.heightAnchor.constraint(equalTo: cell.cellView.heightAnchor),
                                      blurView.widthAnchor.constraint(equalTo: cell.cellView.widthAnchor)])
    }
    func mapViewDidFinishRenderingMap(_ mapView: MKMapView, fullyRendered: Bool) {
        if didAnimateMap == false {
            mapKitViewForAddress(address: self.compAnchor.fullAddress, mapView: mapView)
            Timer.scheduledTimer(withTimeInterval: 1.0/7000.0, repeats: true, block: { (timer) in
                self.mapView.camera.pitch += 0.1
                self.mapView.camera.altitude -= 0.8
                if self.mapView.camera.pitch >= 50 {
                    timer.invalidate()
                }})
            self.didAnimateMap = true
        }
    }
    @objc func handleLongPress(gesture: UILongPressGestureRecognizer!) {
        let press = gesture.location(in: self.collectionView)
        gesture.cancelsTouchesInView = true
        
        if let indexPath = self.collectionView.indexPathForItem(at: press) {
            let comp = generatedComps[indexPath.row]
            let streetAddress = comp.address
            let borough = boroughCodes[comp.borough]
            
            if gesture.state == .began {
                coordinates(forAddress:  streetAddress + " " + borough! + " NYC") { (location) in
                    guard let location = location else {return}
                    let startLocation = CLLocation(latitude: self.compAnchor.lattitude, longitude: self.compAnchor.longitude)
                    let endLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
                    let locations = [startLocation, endLocation]
                    var coordinates = locations.map({(location: CLLocation!) -> CLLocationCoordinate2D in return location.coordinate})
                    
                    // Setup Polyline
                    let polyline = MKPolyline(coordinates: &coordinates, count: locations.count)

                    // Pad map with enough space to see rendered polyline
                    var regionRect = polyline.boundingMapRect
                    let wPadding = regionRect.size.width * 0.55
                    let hPadding = regionRect.size.height * 0.55
                    regionRect.size.width += wPadding
                    regionRect.size.height += hPadding

                    // Center the region on the line
                    regionRect.origin.x -= wPadding / 2
                    regionRect.origin.y -= hPadding / 2
                    self.mapView.setRegion(MKCoordinateRegion(regionRect), animated: true)
                    self.mapView.addOverlay(polyline)
                    self.overlays.insert(polyline, at: 0)
                    addAnnotation(viewcontroller: self, comp: comp, mapView: self.mapView)
                    UIImpactFeedbackGenerator().impactOccurred()
                }
            }
            
            if gesture.state == .ended {
                removeAnnotation(viewController: self, mapView: mapView, comp: comp)
                let compAnchorCenterCoordinates = CLLocationCoordinate2D(latitude: compAnchor.lattitude, longitude: compAnchor.longitude)
                mapView.setCenter( compAnchorCenterCoordinates, animated: true)
                return
            }
        }
    }
}
