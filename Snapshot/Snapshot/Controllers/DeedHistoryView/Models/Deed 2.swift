//
//  Deed.swift
//  Snapshot
//
//  Created by clyon jackson on 10/29/19.
//  Copyright © 2019 Ours Technologies. All rights reserved.
//

import UIKit


class Deeds: Codable {
    var deeds : [Deed]
    
    init(){
        deeds = []
    }
}
class Deed: Codable, Comparable{
    var date : String?
    var deed_document_id : String?
    var parties : [String:Array<String>]
    
    // Comparable Protocols
    static func < (lhs: Deed, rhs: Deed) -> Bool {
        lhs.date! > rhs.date!
    }
    static func == (lhs: Deed, rhs: Deed) -> Bool {
        lhs.date == rhs.date
    }
}





