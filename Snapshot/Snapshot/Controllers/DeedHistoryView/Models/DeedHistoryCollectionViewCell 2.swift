//
//  DeedHistoryCollectionViewCell.swift
//  Snapshot
//
//  Created by clyon jackson on 10/30/19.
//  Copyright © 2019 Ours Technologies. All rights reserved.
//

import UIKit

class DeedHistoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var deedYearLabel: UILabel!
    @IBOutlet weak var party1Label: UILabel!
    @IBOutlet weak var party2Label: UILabel!
}
