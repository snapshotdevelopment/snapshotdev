//
//  deedSearch.swift
//  Snapshot
//
//  Created by clyon jackson on 11/14/19.
//  Copyright © 2019 Ours Technologies. All rights reserved.
//

import Foundation
import Alamofire

//DOB Classification, ACCRIS and DOB urls.
func deedSearch(borough:String, block:String, lot:String, completion: @escaping([Deed])-> Void){
    let parameters: Parameters = ["borough":borough, "block":block, "lot":lot]
    let endpoint = "https://mqrj7gdv87.execute-api.us-east-1.amazonaws.com/dev/DeedSearch"
    _ = AF.request(endpoint, method: .get, parameters:parameters).responseJSON{ response in
        guard let data = response.data else{return}
        do {
            let deedObject = try JSONDecoder().decode([Deed].self, from: data)
            
            completion(deedObject)
            
        } catch {
            debugPrint(error)
        }
    }
}
