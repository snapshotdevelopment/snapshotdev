//
//  DeedHistoryViewController.swift
//  Snapshot
//
//  Created by clyon jackson on 10/30/19.
//  Copyright © 2019 Ours Technologies. All rights reserved.
//

import UIKit

class DeedHistoryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var deedCollectionView: UICollectionView!
    var deedsObject = Deeds()

    override func viewDidLoad() {
        super.viewDidLoad()
        }
    }

 extension DeedHistoryViewController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.deedsObject.deeds = self.deedsObject.deeds.sorted()
        return deedsObject.deeds.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = deedCollectionView.dequeueReusableCell(withReuseIdentifier: "deedCell",
                                                             for: indexPath) as? DeedHistoryCollectionViewCell {
            let date = DateStringConversion()
            let year = date.convert(dateString: deedsObject.deeds[indexPath.row].date!).year?.description
            cell.deedYearLabel.text = year
            cell.party1Label.text = deedsObject.deeds[indexPath.row].parties["1"]![0]
            cell.party2Label.text = deedsObject.deeds[indexPath.row].parties["2"]![0]
            return cell
        }
    }
}


