import UIKit
import MapKit
import MaterialComponents.MaterialActivityIndicator
import MaterialComponents.MDCButton
import Firebase

class MainTableView: UIViewController, MKLocalSearchCompleterDelegate, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var navigationItemBar: UINavigationItem!
    @IBOutlet var mainTableView: UITableView!
    @IBOutlet weak var historyLabelView: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchHistoryTableView: UITableView!
    
    // Actions
    @IBAction func didPressSearchButton(_ sender: Any) {
        
        UIView.animate(withDuration: 0.2,
        animations: {
            self.searchButton.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        },
        completion: { _ in
            UIView.animate(withDuration: 0.2) {
                self.searchButton.transform = CGAffineTransform.identity
            }
        })
        
        UIImpactFeedbackGenerator().impactOccurred()
        searchController.searchBar.becomeFirstResponder()
        searchController.showsSearchResultsController = true

    }

    // MARK: - Properties
    var searchCompleter = MKLocalSearchCompleter()
    var userInterfaceStyle = UIUserInterfaceStyle(rawValue: 1 )
    var timer: Timer?
    var searchHistory = [SearchHistoryLog]()
    var searchHistoryDefaults = SearchHistory().get()
    var searchController: UISearchController!
    let activityIndicator = MDCActivityIndicator()
    let tableViewCellIdentifier = "historyCell"
    
    // MARK: - View Controller Pointers
    var searchResultsController = SearchBarResultsViewController()
    private var resultsTableController = SearchBarResultsViewController()
    let propertyResultsViewController = PropertyDetailsViewController()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Search History
        updateSearchHistoryList()
        
        
        // Register Search History Cell
        let nib = UINib(nibName: "SearchHistoryTableCell", bundle: nil)
        searchHistoryTableView.register(nib, forCellReuseIdentifier: tableViewCellIdentifier)
        
        // Create Instance of ResultsTableController
        let storyboard = UIStoryboard(name: "SearchBarResults", bundle: nil)
        let resultsTableController = storyboard.instantiateViewController(identifier: "SearchBarResults") as SearchBarResultsViewController
        resultsTableController.mainTableViewController = self
        
        insertSearchControllerToNavigationBar()
        insertSnapShotLogoToNavitgationBar()
        configureActivityIndicator()
        searchCompleter.delegate = self
        searchController.searchBar.becomeFirstResponder()
    }

    private func configureActivityIndicator() {
        
        activityIndicator.sizeToFit()
        activityIndicator.backgroundColor = .lightGray
        activityIndicator.frame = .init(x: 0, y: 0, width: 75, height: 75)
        activityIndicator.layer.position = self.view.center
        activityIndicator.layer.position.y-=175
        activityIndicator.layer.cornerRadius = 10
        
    }

    private func insertSearchControllerToNavigationBar() {
        
        // Places search bar inside of navigation bar.
        searchController = UISearchController(searchResultsController: resultsTableController)
        searchController.delegate = self
        searchController.searchBar.autocapitalizationType = .none
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "Enter a NYC address."
        searchController.searchBar.tintColor = .systemBlue
        
        
        navigationItem.searchController = searchController
        navigationItem.hidesBackButton = false
        // Make the search bar always visible.
        navigationItem.hidesSearchBarWhenScrolling = false
//        self.navigationController?.navigationBar.isHidden = false
        
    }

    private func insertSnapShotLogoToNavitgationBar() {
        
        // Top Corner Logo
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height + 100))
        label.text = "snapshot"
        
        let attributedString = NSMutableAttributedString(string: "snapshot")
        attributedString.addAttribute(NSAttributedString.Key.kern, value: -1, range: NSRange(location: 0, length: attributedString.length - 1))
        label.attributedText = attributedString
        
        label.font = UIFont(name: "Futura", size: 22)
        navigationItem.titleView = label
        
        
    }
    
    func updateSearchHistoryList() {
        
        let searchHistoryFromDefaults = Array(searchHistoryDefaults)
        self.searchHistory = searchHistoryFromDefaults
        searchHistoryTableView.reloadData()
        
    }
    
}




