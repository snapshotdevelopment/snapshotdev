//
//  SearchHistoryCell.swift
//  Snapshot
//
//  Created by clyon jackson on 1/20/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import UIKit

class SearchHistoryCell: UITableViewCell {
    
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var searchDate: UILabel!
}
