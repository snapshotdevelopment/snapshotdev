//
//  MainTableView+UISearchBarDelegate.swift
//  Snapshot
//
//  Created by clyon jackson on 3/10/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import Foundation
import UIKit
import MapKit

// MARK: - UISearchBarDelegate
extension MainTableView: UISearchBarDelegate, UISearchControllerDelegate {
    
    func didPresentSearchController(_ searchController: UISearchController) {
        
        searchController.showsSearchResultsController = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        self.searchResultsController.nycResults = []
        self.searchResultsController.configureDataSource()
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        timer?.invalidate()
        searchCompleter.region = .init(center: CLLocationCoordinate2D(latitude: 40.5788, longitude: -73.872), span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
        searchCompleter.queryFragment = searchText
        searchCompleter.resultTypes = .address
        
        
        
        if let resultsTableController = searchController.searchResultsController as? SearchBarResultsViewController {
            resultsTableController.searchCompleter = searchCompleter
            
            self.searchResultsController = resultsTableController
            
            timer = Timer.scheduledTimer(withTimeInterval: 0.8, repeats: false, block: { (timer) in
                resultsTableController.createSnapshot()
            })
            resultsTableController.mainTableViewController = self
        }
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
        guard let address = self.navigationItem.searchController?.searchBar.text else { return }
        let storyboard = UIStoryboard(name: "Results", bundle: nil)
        let resultsViewController = storyboard.instantiateViewController(withIdentifier: "Results") as! PropertyDetailsViewController
        
        // Show Loading Indicator
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        getPropertyCharacteristics(address: address, resultsViewController: self.propertyResultsViewController, searchBar: self.searchResultsController, mainVC: self) {
                                    (PropertyCharacteristics) -> PropertyCharacteristics in
            resultsViewController.results = PropertyCharacteristics
            resultsViewController.map?.setCenter(CLLocationCoordinate2D(latitude: PropertyCharacteristics.lattitude, longitude: PropertyCharacteristics.longitude), animated:  true)

            // Update Search History
            SearchHistory().update(fullAddress: PropertyCharacteristics.fullAddress, borough: PropertyCharacteristics.boro)
            self.searchHistoryDefaults = SearchHistory().get()
            self.updateSearchHistoryList()
            
            self.activityIndicator.stopAnimating()
            self.activityIndicator.removeFromSuperview()
            
            // If search is initiated from autocomplete. Handle animation in searchbar results view controller.
            if self.searchResultsController.activityIndicator.isAnimating {
                let searchBarActivityIndicator = self.searchResultsController.activityIndicator
                searchBarActivityIndicator.stopAnimating()
                searchBarActivityIndicator.removeFromSuperview()
            }
            
            self.searchHistoryTableView.reloadData()
            self.searchController.dismiss(animated: true) {
                self.navigationController?.pushViewController(resultsViewController, animated: true)
                self.searchController.searchBar.text = ""
            }
            
            return PropertyCharacteristics
        }
        
    }
}
