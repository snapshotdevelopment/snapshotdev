//
//  SaveData.swift
//  Snapshot
//
//  Created by clyon jackson on 11/29/19.
//  Copyright © 2019 Ours Technologies. All rights reserved.

import Foundation

class SearchHistory {

    func create() {
       let searchHistory = UserDefaults.standard.dictionary(forKey: "SearchHistory")
        if searchHistory == nil {
            let defaults = UserDefaults.standard
            defaults.set( [String:[String]](), forKey: "SearchHistory")
        }
    }
    
    func update(fullAddress: String, borough: String) {
        
        // Clean up address
        var address = fullAddress
        if let range = address.range(of: ", USA") {
          address.removeSubrange(range)
        }
        
        // Save
        var newEntry : [String: [Any]]
        if let database = UserDefaults.standard.dictionary(forKey: "SearchHistory") as? [String: [Any]] {
            newEntry = database
        } else {
            newEntry = [:]
        }
        newEntry[address] = [Date(), borough]
        UserDefaults.standard.set(newEntry, forKey: "SearchHistory")
        
    }
    
    func get() -> [SearchHistoryLog] {
        
        var historyItems = [SearchHistoryLog]()
        let defaults = UserDefaults.standard
        
        if let searchHistory = defaults.value(forKey: "SearchHistory") as? [String:[Any]] {
            for item in searchHistory {
                let searchItem = SearchHistoryLog(item: (address: item.key, date: item.value[0] as! Date, borough: item.value[1] as! String))
                historyItems.append(searchItem)
            }
        }
        
        return historyItems.sorted().reversed()
    }
    
}

struct SearchHistoryLog: Comparable {
    
    static func < (lhs: SearchHistoryLog, rhs: SearchHistoryLog) -> Bool {
        lhs.item.date < rhs.item.date
    }
    
    static func == (lhs: SearchHistoryLog, rhs: SearchHistoryLog) -> Bool {
        lhs.item.address == rhs.item.address
    }
    
    let item : (address: String, date: Date, borough: String)
}
