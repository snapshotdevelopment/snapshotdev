import UIKit
import MapKit
import MaterialComponents.MaterialActivityIndicator

class MainTableView: UITableView, MKLocalSearchCompleterDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var navigationItemBar: UINavigationItem!
    var searchCompleter = MKLocalSearchCompleter()
    @IBOutlet var mainTableView: UITableView!
    
    // MARK: - Properties
    
    // View Controller Pointers
    let searchResultsController = SearchBarResultsViewController()
    private var resultsTableController = SearchBarResultsViewController()
    let propertyResultsViewController = ResultsTableController()
    let activityIndicator = MDCActivityIndicator()
    
    let tableViewCellIdentifier = "historyCell"
    
    // Data model for search History
    var searchHistory = [String]()
    var searchHistoryDefaults = [String:String]()
    
    // Search Controller for filtering items in the table view
    var searchController : UISearchController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Search History
        searchHistoryDefaults = SearchHistory().load()
        updateSearchHistoryList()
        
        // Make the search bar always visible.
        self.navigationItem.hidesSearchBarWhenScrolling = false
        
        // Register Search History Cell
        let nib = UINib(nibName: "SearchHistoryTableCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: tableViewCellIdentifier)
        
        // Create Instance of ResultsTableController
        let storyboard = UIStoryboard(name: "SearchBarResults", bundle: nil)
        let resultsTableController = storyboard.instantiateViewController(identifier:"SearchBarResults") as! SearchBarResultsViewController
        resultsTableController.mainTableViewController = self
        
        // Place search bar inside of navigation bar.
        searchController = UISearchController(searchResultsController: resultsTableController)
        searchController.delegate = self
        searchController.searchBar.autocapitalizationType = .none
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "Type an address. We'll do the rest."
        navigationItem.searchController = searchController
        
        // Top Corner Logo
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 32, height: view.frame.height + 100))
        label.text = "snapshot"
        let attributedString = NSMutableAttributedString(string: "snapshot")
        attributedString.addAttribute(NSAttributedString.Key.kern, value: -1, range: NSRange(location: 0, length: attributedString.length - 1))
        label.attributedText = attributedString
        label.font = UIFont(name: "Futura", size: 25)
        navigationItem.titleView = label
        
        activityIndicator.sizeToFit()
        activityIndicator.layer.position = view.center
        view.addSubview(activityIndicator)
        
        searchCompleter.delegate = self
    }
}

extension MainTableView {
    // MARK: - Results Table Generator
    private func resultsTableControllerForAddress(_ address: String) {
        self.activityIndicator.startAnimating()
        let storyboard = UIStoryboard(name: "Results", bundle: nil)
        let viewController =
            storyboard.instantiateViewController(withIdentifier: "Results") as! ResultsTableController
        
        if let resultsViewController = viewController as? ResultsTableController {
            resultsViewController.addressHeaderLabel?.text = address
            getPropertyCharacteristics(address: address, resultsViewController: resultsViewController,  mainVC: self) { (PropertyCharacteristics) in
                resultsViewController.results = PropertyCharacteristics
                self.activityIndicator.stopAnimating()
                
                DispatchQueue.main.async {
                    resultsViewController.tableView.reloadData()
                    resultsViewController.addressHeaderLabel?.text = PropertyCharacteristics.fullAddress
                }
                
                SearchHistory().update(fullAddress: PropertyCharacteristics.fullAddress)
                self.searchHistoryDefaults = SearchHistory().load()
                self.updateSearchHistoryList()
                self.tableView.reloadData()
                self.navigationController?.pushViewController(resultsViewController, animated: true)
                return PropertyCharacteristics
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension MainTableView {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let address = searchHistory[indexPath.row]
        resultsTableControllerForAddress(address)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyCell")!
        let address = self.searchHistory[indexPath.row]
        cell.textLabel!.text = address
        cell.detailTextLabel!.text = self.searchHistoryDefaults[address]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchHistory.count
    }
}


// MARK: - UISearchBarDelegate
extension MainTableView: UISearchBarDelegate, UISearchControllerDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchCompleter.region = .init(center: CLLocationCoordinate2D(latitude: 40.5788, longitude: -73.872), span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
        searchCompleter.queryFragment = searchText
        searchCompleter.resultTypes = .address
        
        if let resultsTableController = searchController.searchResultsController as? SearchBarResultsViewController {
            resultsTableController.searchCompleter = searchCompleter
            resultsTableController.createSnapshot()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        guard let address = self.navigationItem.searchController?.searchBar.text else { return }
        let storyboard = UIStoryboard(name: "Results", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "Results") as! ResultsTableController
        
        // Show Loading Indicator
        self.activityIndicator.startAnimating()

        getPropertyCharacteristics(address: address, resultsViewController: self.propertyResultsViewController, mainVC: self) {
            (PropertyCharacteristics) -> PropertyCharacteristics in
            
            viewController.results = PropertyCharacteristics
            viewController.map?.setCenter(CLLocationCoordinate2D(latitude: PropertyCharacteristics.lattitude,longitude: PropertyCharacteristics.longitude), animated:  true)

            // Update Search History
            SearchHistory().update(fullAddress: PropertyCharacteristics.fullAddress)
            self.searchHistoryDefaults = SearchHistory().load()
            self.updateSearchHistoryList()
            
            // To make the activity indicator disappear:
            self.activityIndicator.stopAnimating()
            
            self.tableView.reloadData()
            self.navigationController?.pushViewController(viewController, animated: true)
            return PropertyCharacteristics
        }
    }
    
    private func updateSearchHistoryList() {
        let defaults =  self.searchHistoryDefaults
        let searchHistoryFromDefaults = Array(defaults.keys).sorted(by: {defaults[$0].hashValue < defaults[$1].hashValue})
        self.searchHistory = Array(searchHistoryFromDefaults.reversed())
        self.tableView.reloadData()
    }
}

