//
//  MainTableView+TableViewDataSource.swift
//  Snapshot
//
//  Created by clyon jackson on 3/10/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import Foundation
import UIKit
import MapKit

// MARK: - UITableViewDataSource
extension MainTableView {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        UIImpactFeedbackGenerator().impactOccurred()
 
        let searchHistoryItem = searchHistory[indexPath.row]
        self.searchController.searchBar.text = searchHistoryItem.item.address
        
        if let cachedProperty = cache.object(forKey: searchHistoryItem.item.address + ", USA" as NSString) {
            let storyboard = UIStoryboard(name: "Results", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "Results") as! PropertyDetailsViewController
            viewController.results = cachedProperty
            viewController.map?.setCenter(CLLocationCoordinate2D(latitude: cachedProperty.lattitude,longitude: cachedProperty.longitude), animated:  true)
            self.searchController.searchBar.text = ""
            self.navigationController?.pushViewController(viewController, animated: true)
        } else {
            searchBarSearchButtonClicked(self.searchController.searchBar)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let boroughCodes = ["1": "MAN", "2": "BX", "3": "BK", "4": "QNS" , "5": "SI"]
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "historyCell") as? SearchHistoryCell {
            let searchHistoryItem = self.searchHistory[indexPath.row]
            let borough = boroughCodes[searchHistoryItem.item.borough]
            let date = searchHistoryItem.item.date.localizedDescription
            
            MapKitFunctions().getSubLocality(forAddress: searchHistoryItem.item.address, completion: { (result) in
                
                switch result {
                    case .failure:
                        cell.boroughLabel.text = ""
                    
                    case .success:
                        cell.address.text = searchHistoryItem.item.address
                        cell.searchDate.text = date
                        cell.boroughLabel.text = borough
                        cell.boroughLabel.textColor = .tertiaryLabel
                }})
            return cell
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "historyCell")
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchHistory.count
    }
    
}
