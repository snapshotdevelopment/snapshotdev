//
//  TickerCollectionViewCell.swift
//  Snapshot
//
//  Created by clyon jackson on 4/20/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import UIKit

class TickerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var borough: UILabel!
    @IBOutlet weak var salePrice: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var transactionTypeImage: UIImageView!

}
