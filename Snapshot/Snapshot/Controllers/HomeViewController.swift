//
//  HomeViewController.swift
//  Snapshot
//
//  Created by clyon jackson on 4/20/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestoreSwift
import MaterialComponents.MaterialActivityIndicator
import MapKit

class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, MKLocalSearchCompleterDelegate {
    
    @IBOutlet weak var swipeDownLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var logo: UILabel!
    
    @IBAction func didSwipeDown(_ sender: Any) {
        
        UIImpactFeedbackGenerator().impactOccurred(intensity: 1)
        let searchBar = UISearchBar()
        searchBar.sizeToFit()
        self.navigationItem.titleView = searchBar
        let storyboard = UIStoryboard(name: "MainTableView", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(identifier: "MainTableView") as! MainTableView
        self.navigationController?.pushViewController(mainViewController, animated: false)
        self.pauseAutoScroll = true
        
    }
    

    let useStaticData = false
    var recentSales = [RecentSale]()
    var pauseAutoScroll = false
    
    // Search Delegate
    var searchResultsController = SearchBarResultsViewController()
    var timer = Timer()
    let activityIndicator = MDCActivityIndicator()
    var searchCompleter = MKLocalSearchCompleter()
    var mainViewController = MainTableView()
    
    // Properties for transition decision
    var shouldTransitionToSearchController = true
    
    var cellWidth = 0
    var resetTicker = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
//        if self.pauseAutoScroll == false {
//            autoScroll()
//        }
//
        logo.textColor = .label
        self.swipeDownLabel.layer.cornerRadius = 10
        
        db.collection("sales").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    let sale = RecentSale(bbl: document.data()["bbl"] as! String,
                                          price: document.data()["transaction_amount"] as! Int,
                                          borough: "QN",
                                          address: document.data()["address"] as! String,
                                          party1: document.data()["party1"] as! String,
                                          party2: document.data()["party2"] as! String)
                    self.recentSales.append(sale)
                    print(sale)
                }
                self.recentSales = self.recentSales.filter{ $0.party2.contains("LLC") || $0.party2.contains("CORP") || $0.party2.contains(" INC") || $0.party2.contains(" INCORPORATED") }
                self.collectionView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.pauseAutoScroll = false
        print(autoScroll())
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recentSales.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let nib = UINib(nibName: "TickerCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "tickerCell")

        if let cell :TickerCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "tickerCell", for: indexPath) as? TickerCollectionViewCell {
            var salePrice = recentSales[indexPath.row].price.formattedWithSeparator
            if let range = salePrice.range(of: ".00") {
                salePrice.removeSubrange(range)
            }
            cell.address?.text = recentSales[indexPath.row].address
            cell.salePrice?.text = salePrice.description
            cell.borough?.text = "QN"
            cell.layer.cornerRadius = 6
            
            let layer = cell.cellView.layer
            layer.masksToBounds = false
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOpacity = 1
            layer.shadowOffset = .zero
            layer.shadowRadius = 10
            layer.shouldRasterize = true
            layer.rasterizationScale = UIScreen.main.scale
            return cell
            
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tickerCell", for: indexPath) as! TickerCollectionViewCell
            cell.address.text = recentSales[indexPath.row].bbl
            return cell
            
        }
    }
    
     func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        cell.alpha = 0
        UIView.animate(withDuration: 1.2) {
                cell.alpha = 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        
        timer.invalidate()
        self.pauseAutoScroll = true
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        
        self.pauseAutoScroll = false
        self.autoScroll()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if indexPath.row == recentSales.count - 1 {
            self.resetTicker = true
        }
    }
    
    func autoScroll () {
        let contentOffset = collectionView.contentOffset.x
        print(contentOffset)
        let no = contentOffset + 1
    
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.001, delay: 0, options: [.allowUserInteraction, .curveEaseInOut], animations: { [weak self]() -> Void in
                self?.collectionView.contentOffset = CGPoint(x: no, y: 0)}) { [weak self](finished) -> Void in
                    
                    if self?.resetTicker == true {
                        self?.collectionView.contentOffset = CGPoint(x: -500, y: 0)
                        self?.resetTicker = false
                    }
                    
                    if self?.pauseAutoScroll == false {
                        self?.autoScroll()
                    }
                }
            }
        }
}

struct RecentSale {
    
    var bbl: String
    var price: Int
    var borough: String
    var address: String
    var party1: String
    var party2: String
}

extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.numberStyle = .currency
        formatter.generatesDecimalNumbers = false
        return formatter
    }()
}

extension Numeric {
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
}
