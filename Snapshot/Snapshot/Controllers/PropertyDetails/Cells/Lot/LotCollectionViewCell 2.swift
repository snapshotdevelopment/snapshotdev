//
//  LotCollectionViewCell.swift
//  Snapshot
//
//  Created by clyon jackson on 12/19/19.
//  Copyright © 2019 Ours Technologies. All rights reserved.
//

import UIKit

class LotCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lotWidthLabel: UILabel!
    @IBOutlet weak var lotDepthLabel: UILabel!
    @IBOutlet weak var lotSQFTLabel: UILabel!
    @IBOutlet weak var categoryIcon: UIImageView!
}
