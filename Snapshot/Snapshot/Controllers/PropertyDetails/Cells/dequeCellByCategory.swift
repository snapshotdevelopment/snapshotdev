//
//  dequeLotCell.swift
//  Snapshot
//
//  Created by clyon jackson on 12/20/19.
//  Copyright © 2019 Ours Technologies. All rights reserved.
//
import UIKit

func dequeCellByCategory(category: String, collectionView:UICollectionView, propertyDetails:[[PropertyDescriptor]], indexPath:IndexPath) -> UICollectionViewCell {
    
    switch category {
    case "Lot":
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "lotCell", for: indexPath as IndexPath) as! LotCollectionViewCell
        let specs = propertyDetails[indexPath.row]
        let lotWidth = specs[0].descriptor
        let lotDepth = specs[1].descriptor
        let sqft = specs[2].descriptor
        
        cell.lotWidthLabel.text = lotDepth.value
        cell.lotDepthLabel.text = lotWidth.value
        cell.lotSQFTLabel.text = sqft.value
        cell.layer.cornerRadius = 10
        return cell
        
    case "Classification":
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bcCell", for: indexPath as IndexPath) as! BuildingClassificationCollectionViewCell
        let specs = propertyDetails[indexPath.row]
        let units = specs[0].descriptor
        let zone = specs[1].descriptor
        let buildingClass = specs[2].descriptor
        cell.unitsLabel.text = units.value
        cell.zoningLabel.text = zone.value
        cell.classificationLabel.text = buildingClass.value
        cell.layer.cornerRadius = 10
        cell.clipsToBounds = true
        return cell
    
    default:
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pdCell", for: indexPath as IndexPath) as! PropertyDetailsCollectionViewCell
        cell.label.text = propertyDetails[indexPath.row][1].descriptor.value
        cell.value.text = propertyDetails[indexPath.row][1].descriptor.labelName
        cell.layer.cornerRadius = 10
        return cell
    }
}


