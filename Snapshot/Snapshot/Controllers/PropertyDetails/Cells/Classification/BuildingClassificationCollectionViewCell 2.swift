//
//  BuildingClassificationCollectionViewCell.swift
//  Snapshot
//
//  Created by clyon jackson on 12/19/19.
//  Copyright © 2019 Ours Technologies. All rights reserved.
//

import UIKit

class BuildingClassificationCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var unitsLabel: UILabel!
    @IBOutlet weak var zoningLabel: UILabel!
    @IBOutlet weak var classificationLabel: UILabel!
    @IBOutlet weak var categoryIcon: UIImageView!
}
