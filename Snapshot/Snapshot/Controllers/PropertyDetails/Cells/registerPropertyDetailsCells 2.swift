//
//  registerCells.swift
//  Snapshot
//
//  Created by clyon jackson on 12/20/19.
//  Copyright © 2019 Ours Technologies. All rights reserved.
//

import UIKit

func registerPropertyDetailsCells(collectionView:UICollectionView) {
    collectionView.register(UINib(nibName: "BuildingClassificationCell", bundle: .main), forCellWithReuseIdentifier: "bcCell")
    collectionView.register(UINib(nibName: "LotCell", bundle: .main), forCellWithReuseIdentifier: "lotCell")
}
