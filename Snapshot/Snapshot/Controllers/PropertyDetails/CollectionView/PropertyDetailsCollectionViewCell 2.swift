//
//  PropertyDetailsCollectionViewCell.swift
//  Snapshot
//
//  Created by clyon jackson on 11/21/19.
//  Copyright © 2019 Ours Technologies. All rights reserved.
//

import Foundation
import UIKit

class PropertyDetailsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var icon: UIImageView!
}
