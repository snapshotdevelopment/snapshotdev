//
//  AutoCompleteCell.swift
//  Snapshot
//
//  Created by Clyon Jackson on 3/31/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import UIKit

class AutoCompleteCell: UITableViewCell {
    
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var neighborhoodLabel: UILabel!
    @IBOutlet weak var searchIcon: UIImageView!
    @IBOutlet weak var inputIcon: UIImageView!
    
}
