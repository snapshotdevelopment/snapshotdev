//
//  MainViewDataSource.swift
//  Snapshot
//
//  Created by clyon jackson on 3/5/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import UIKit

struct Auction {
    
    let address:String
    let lien:Int
}

enum Section:Hashable {
    
    case searchResults
    case upcomingAuctions
    
}

func getSections()->[Any] {
    
    let searchHistory = SearchHistory().get()
    let auctions = [Auction(address: "2916 Falcon Ave, Far Rockaway", lien: 899000), Auction(address: "88 Saratoga Ave", lien: 250000)]
    let sections = [searchHistory, auctions] as [Any]
    return sections
    
}
