//
//  SearchBarResultsViewController.swift
//  Snapshot
//
//  Created by clyon jackson on 3/5/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import UIKit
import MapKit
import MaterialComponents.MaterialActivityIndicator

class SearchBarResultsViewController: UITableViewController, MKLocalSearchCompleterDelegate {
    
    var dataSource: UITableViewDiffableDataSource<Section, MKLocalSearchCompletion>!
    var searchCompleter = MKLocalSearchCompleter()
    var nycResults : [MKLocalSearchCompletion] = []
    var mainTableViewController : MainTableView!
    let activityIndicator = MDCActivityIndicator()
    var noDataLabel = UILabel()
    var timer: Timer?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "Suggestions"
        tableView.register(UINib(nibName: "AutoCompleteCell", bundle: .main), forCellReuseIdentifier: "cell")
        configureDataSource()
        configureActivityIndicator()
        searchCompleter.delegate = self
        addResultTableViewPromptLabel()
        
    }
    
    @objc func createSnapshot() {
        
        self.nycResults = [MKLocalSearchCompletion]()
        var snapshot = NSDiffableDataSourceSnapshot<Section, MKLocalSearchCompletion>()
        snapshot.appendSections([.searchResults, .upcomingAuctions])
        
        for suggestion in searchCompleter.results {
            let address = suggestion.title + " " + suggestion.subtitle
            
            MapKitFunctions().getSubLocality(forAddress:address) { result in
                
                switch result {
                    case .failure:
                        snapshot.appendItems(self.nycResults, toSection: .searchResults)
                        self.dataSource.apply(snapshot, animatingDifferences: false)
                        if self.nycResults.count == 0 {
                            self.noDataLabel.text = "No suggestions available."
                        }
                    
                    case .success:
                        self.noDataLabel.text = ""
                        self.nycResults.append(suggestion)
                        // Update Datasource
                        snapshot.appendItems(self.nycResults, toSection: .searchResults)
                        self.dataSource.apply(snapshot, animatingDifferences: false)
                }
                
            }
        }
        
    }
    
    func configureDataSource() {
        
        dataSource = UITableViewDiffableDataSource<Section, MKLocalSearchCompletion> (tableView:tableView) { (tableView, indexPath, suggestion) -> UITableViewCell? in
            let index = indexPath.row
            if index >= 0 && index < self.nycResults.count {
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AutoCompleteCell
                cell.detailLabel.text = self.nycResults[indexPath.row].title
                cell.neighborhoodLabel.text = self.nycResults[indexPath.row].subtitle
                cell.searchIcon.tintColor = .snapshotBlue
                cell.inputIcon.tintColor = .snapshotBlue
                return cell
            }else{
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AutoCompleteCell
                cell.detailLabel.text = ""
                cell.neighborhoodLabel.text = ""
                return cell
            }
        }
    }

    // TableView protocols
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let address = self.nycResults[indexPath.row].title + " " + self.nycResults[indexPath.row].subtitle
        self.mainTableViewController.searchController.searchBar.text = address
        
        self.mainTableViewController.searchResultsController = self
        
        // Add activity Indicator
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        mainTableViewController.searchBarSearchButtonClicked(mainTableViewController.searchController.searchBar)
        
    }
    
    private func configureActivityIndicator() {
        
        activityIndicator.sizeToFit()
        activityIndicator.backgroundColor = .lightGray
        activityIndicator.frame = .init(x: 0, y: 0, width: 75, height: 75)
        activityIndicator.layer.position = self.view.center
        activityIndicator.layer.position.y -= 175
        activityIndicator.layer.cornerRadius = 10
        
    }
    
    func addResultTableViewPromptLabel() {
        
        noDataLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height))
        
        self.noDataLabel.text = "SEARCH"
        self.noDataLabel.textColor = .tertiaryLabel
        self.noDataLabel.textAlignment = .center
        self.noDataLabel.font = UIFont(name: "Helvetica-Bold", size: 16)
        self.tableView.backgroundView  = self.noDataLabel
        self.tableView.backgroundColor = .systemBackground
        self.tableView.separatorStyle  = .none
        
    }
        
}

