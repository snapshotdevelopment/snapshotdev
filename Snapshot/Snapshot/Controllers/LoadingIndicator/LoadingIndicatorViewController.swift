//
//  LoadingIndicatorViewController.swift
//  Snapshot
//
//  Created by clyon jackson on 11/24/19.
//  Copyright © 2019 Ours Technologies. All rights reserved.
//


import UIKit
import MaterialComponents.MaterialActivityIndicator

class LoadingIndicatorViewController: UIViewController {
    
    @IBOutlet weak var indicatorChildView: UIView!
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let activityIndicator = MDCActivityIndicator()
        activityIndicator.sizeToFit()
        self.indicatorChildView.addSubview(activityIndicator)
        
        // To make the activity indicator appear:
        activityIndicator.startAnimating()
        self.indicatorChildView.frame.origin = CGPoint(x: 0, y: 0)
    }
}
