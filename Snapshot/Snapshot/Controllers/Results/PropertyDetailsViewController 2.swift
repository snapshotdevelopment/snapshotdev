//
//  ResultsTableView.swift
//  Snapshot
//
//  Created by clyon jackson on 1/20/20.
//  Copyright © 2020 Ours Technologies. All rights reserved.
//

import UIKit
import MapKit

class PropertyDetailsViewController: UITableViewController {
    // MARK: - Properties
    var comparablesViewController = ComparableViewController()
    let tableViewCellIdentifier = "resultsCell"
    var results = PropertyCharacteristics(zone: "", accrisURL: "", dobURL: "", marketValue: "", owner: "", yearBuilt: "", units: "", lotFront: "", lotDepth: "", landArea: "", buidlingClass: "", buildingCount: "", stories: "", buildingDimensionFront: "", buildingDimensionDepth: "", buildingSQFT: "", boro: "", block: "", lot: "", fullAddress: "", neighborhood: "", lattitude: 0.0, longitude: 0.0, comps: [])
    
    
    // MARK: - Outlets
    @IBOutlet weak var compCountLabel: UILabel?
    @IBOutlet weak var addressHeaderLabel: UILabel?
    @IBOutlet weak var compButton: UIButton?
    @IBOutlet weak var map: MKMapView?
    @IBAction func compButton(_ sender: Any) {
        let impact = UIImpactFeedbackGenerator()
        impact.impactOccurred()
        comparablesViewController = comparablesForAddress(address: self.results.fullAddress, comparables: self.results.comps as? [Comp] ?? [])
        DispatchQueue.main.async {
            self.comparablesViewController.compAnchor = self.results
            self.navigationController?.pushViewController(self.comparablesViewController, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.map != nil {
            mapKitViewForAddress(address: self.results.fullAddress, mapView: self.map!)
        }
        
        if self.compButton != nil {
            self.compButton?.layer.cornerRadius = 10
            self.compButton?.backgroundColor = .fiyah
        }
        self.addressHeaderLabel?.text = self.results.fullAddress
        compCountLabel?.text = results.comps.count.description
        
        if self.results.comps.count == 0 {
            self.compButton?.isEnabled = false
        }
    }
    
    // MARK: - UITableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.results.propertyDetailsDescriptors()[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let nib = UINib(nibName: "ResultsTableCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: tableViewCellIdentifier)
        let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellIdentifier, for: indexPath)
    
        cell.textLabel?.text = results.propertyDetailsDescriptors()[indexPath.section][indexPath.row].descriptor.labelName
        cell.detailTextLabel?.text = results.propertyDetailsDescriptors()[indexPath.section][indexPath.row].descriptor.value
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        self.results.propertyDetailsDescriptors().count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.results.propertyDetailsDescriptors()[section][0].descriptor.category
    }
}
