//
//  DOBPropertyData.swift
//  Snapshot
//
//  Created by clyon jackson on 9/22/19.
//  Copyright © 2019 Ours Technologies. All rights reserved.
//

import UIKit

class PropertyCharacteristics: Decodable {
    // Access to the variables
    let zone : String
    let accrisURL: String
    let dobURL : String
    let marketValue : String
    let owner : String
    let yearBuilt : String
    let units : String
    let lotFront : String
    let lotDepth : String
    let landArea : String
    let buildingClass : String
    let buildingCount : String
    let stories : String
    let buildingDimensionFront : String
    let buildingDemtionDepth : String
    let buildingSQFT : String
    let boro : String
    let block : String
    let lot : String
    var fullAddress : String
    let neighborhood : String
    let lattitude : Double
    let longitude : Double
    var comps : [Any]

//    init(from dictionary: [String: Any]) throws {
//        guard let zone = dictionary["zone"] as? String,
//            let accrisURL = dictionary["accrisURL"] as? String,
//            let dobURL = dictionary["dobURL"] as? String,
//            let marketValue = dictionary["marketValue"] as? String,
//            let owner = dictionary["owner"] as? String,
//            let yearBuilt = dictionary["yearBuilt"] as? String,
//            let units = dictionary["units"] as? String,
//            let lotFront = dictionary["lotFront"] as? String,
//            let lotDepth = dictionary["lotDepth"] as? String,
//            let landArea = dictionary["landArea"] as? String,
//            let buildingClass = dictionary["buildingClass"] as? String,
//            let buildingCount = dictionary["buildingCount"] as? String,
//            let stories = dictionary["stories"] as? String,
//            let buildingDimensionFront = dictionary["buildingDimensionFront"] as? String,
//            let buildingDemtionDepth = dictionary["buildingDemtionDepth"] as? String,
//            let buildingSQFT = dictionary["buildingSQFT"] as? String,
//            let boro = dictionary["boro"] as? String,
//            let block = dictionary["block"] as? String,
//            let lot = dictionary["lot"] as? String,
//            let fullAddress = dictionary["fullAddress"] as? String,
//            let neighborhood = dictionary["neighborhood"] as? String ,
//            let lattitude = dictionary["neighborhood"] as? Double,
//            let longitude = dictionary["longitude"] as? Double,
//            let comps = dictionary["comps"] as? [Any] else {
//                log.debug("Emoji.beetle")
//                throw NSError(domain: "Property Characteristic Init Failed", code: 100, userInfo: nil)
//        }
//
//        self.zone = zone
//        self.accrisURL = accrisURL
//        self.dobURL = dobURL
//        self.marketValue = marketValue
//        self.owner = owner
//        self.yearBuilt = yearBuilt
//        self.units = units
//        self.lotFront = lotFront
//        self.lotDepth = lotDepth
//        self.landArea = landArea
//        self.buildingClass = buildingClass
//        self.buildingCount = buildingCount
//        self.stories = stories
//        self.buildingDimensionFront = buildingDimensionFront
//        self.buildingDemtionDepth = buildingDemtionDepth
//        self.buildingSQFT = buildingSQFT
//        self.boro = boro
//        self.block = block
//        self.lot = lot
//        self.fullAddress = fullAddress
//        self.neighborhood = neighborhood
//        self.lattitude = lattitude
//        self.longitude = longitude
//        self.comps = comps
//    }
//
//    var dictionary: [String: Any] {
//        return ["zone": zone,
//                "accrisURL": accrisURL,
//                "dobURL": dobURL,
//                "marketValue": marketValue,
//                "owner": owner,
//                "yearBuilt": yearBuilt,
//                "units": units,
//                "lotFront": lotFront,
//                "lotDepth": lotDepth,
//                "landArea": landArea,
//                "buildingClass": buildingClass,
//                "buildingCount": buildingClass,
//                "stories": stories,
//                "buildingDimensionFront": buildingDimensionFront,
//                "buildingDemtionDepth": buildingDemtionDepth,
//                "buildingSQFT": buildingSQFT,
//                "boro": boro,
//                "block": block,
//                "lot": lot,
//                "fullAddress": fullAddress,
//                "neighborhood": neighborhood,
//                "lattitude": lattitude,
//                "longitude": longitude,
//                "comps": comps]
//    }
    
    //    First Initializer
    init(zone:String, accrisURL:String, dobURL:String, marketValue:String, owner:String, yearBuilt:String, units:String, lotFront:String, lotDepth:String, landArea:String, buidlingClass:String, buildingCount:String, stories:String, buildingDimensionFront:String, buildingDimensionDepth:String, buildingSQFT:String, boro:String, block:String, lot:String, fullAddress:String, neighborhood:String, lattitude:Double, longitude:Double, comps: [Any]) {
        
        self.zone = zone
        self.accrisURL = accrisURL
        self.dobURL = dobURL
        self.marketValue = marketValue
        self.owner = owner
        self.yearBuilt = yearBuilt
        self.units = units
        self.lotFront = lotFront
        self.lotDepth = lotDepth
        self.landArea = landArea
        self.buildingClass = buidlingClass
        self.buildingCount = buildingCount
        self.stories = stories
        self.buildingDimensionFront = buildingDimensionFront
        self.buildingDemtionDepth = buildingDimensionDepth
        self.buildingSQFT = buildingSQFT
        self.boro = boro
        self.block = block
        self.lot = lot
        self.fullAddress = fullAddress
        self.neighborhood = neighborhood
        self.lattitude = Double(lattitude)
        self.longitude = longitude
        self.comps = comps
    }

    func propertyDetailsDescriptors() -> [[PropertyDescriptor]] {
        let descriptors = [
                            [
                                PropertyDescriptor(descriptor: (labelName: "Units" , value: units, category: "Classification", icon: "units")),
                                PropertyDescriptor(descriptor: (labelName: "Zone", value: zone, category: "Classification", icon: "Zone")),
                                PropertyDescriptor(descriptor: (labelName: "Building Class", value: buildingClass, category:"Classification", icon:"")),
                                PropertyDescriptor(descriptor: (labelName: "Owner", value: owner, category:"Classification", icon: "owner" ))
                            ],
                            
                            [
                                 PropertyDescriptor(descriptor: (labelName: "Lot Width", value: lotFront, category:"Lot", icon: "Lot Width")),
                                 PropertyDescriptor(descriptor: (labelName: "Lot Depth", value: lotDepth, category:"Lot", icon: "Lot Depth")),
                                 PropertyDescriptor(descriptor: (labelName: "Land Area", value: landArea, category:"Lot", icon: "lotArea"))
                            ],
                            
                            [
                                PropertyDescriptor(descriptor: (labelName: "Building Front", value: buildingDimensionFront, category:"Building", icon: "Building Front")),
                                PropertyDescriptor(descriptor: (labelName: "Building Depth", value: buildingDemtionDepth, category:"Building", icon: "Building Depth")),
                                PropertyDescriptor(descriptor: (labelName: "Building Count", value: buildingCount, category:"Building", icon: "Building Count")),
                                PropertyDescriptor(descriptor: (labelName: "Building SQFT" , value: buildingSQFT, category:"Building", icon: "Building SQFT")),
                                PropertyDescriptor(descriptor: (labelName: "Stories", value: stories, category:"Building", icon: "Storie")),
                            ],
                            
                            [
                                PropertyDescriptor(descriptor: (labelName: "YearBuilt", value: yearBuilt, category:"All", icon: "calendar")),
                                PropertyDescriptor(descriptor: (labelName: "Market Value", value: marketValue, category:"All", icon: "marketValue"))
                            ]
                    ]
        return descriptors
    }

    enum PropertyCharacteristicsKeys: String, CodingKey {
        // declarekeys
        case zone = "zone"
        case id = "id"
        case twitter = "twitter"
        case accrisURL = "accris_link"
        case dobURL = "dob_link"
        case marketValue = "market_value"
        case owner = "owner"
        case yearBuilt = "year_built"
        case units = "units"
        case lotFront = "lot_front"
        case lotDepth = "lot_depth"
        case landArea = "land_area"
        case buidlingClass = "buidling_class"
        case buildingCount = "number_of_buildings"
        case stories = "stories"
        case buildingFront = "building_front"
        case buildingDepth = "building_depth"
        case buildingSQFT = "building_sqft"
        case boro = "boro"
        case block = "block"
        case lot = "lot"
        case fullAddress = "full_address"
        case neighborhood = "neighborhood"
        case lattitude = "lattitude"
        case longitude = "longitude"
        case comps = "comps"
    }
    // Second Initializer from trying to decode the JSON
    required convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: PropertyCharacteristicsKeys.self)
        let zone = try container.decode(String.self, forKey: .zone)
        let accrisURL = try container.decode(String.self, forKey: .accrisURL)
        let dobURL = try container.decode(String.self, forKey: .dobURL)
        let marketValue = try container.decode(String.self, forKey: .marketValue)
        let owner = try container.decode(String.self, forKey: .owner)
        let yearBuilt = try container.decode(String.self, forKey: .yearBuilt)
        let units = try container.decode(String.self, forKey: .units)
        let lotFront = try container.decode(String.self, forKey: .lotFront)
        let lotDepth = try container.decode(String.self, forKey: .lotDepth)
        let landArea = try container.decode(String.self, forKey: .landArea)
        let buidlingClass = try container.decode(String.self, forKey: .buidlingClass)
        let buildingCount = try container.decode(String.self, forKey: .buildingCount)
        let stories = try container.decode(String.self, forKey: .stories)
        let buildingFront = try container.decode(String.self, forKey: .buildingFront)
        let buildingDepth = try container.decode(String.self, forKey: .buildingDepth)
        let buildingSQFT = try container.decode(String.self, forKey: .buildingSQFT)
        let boro = try container.decode(String.self, forKey: .boro)
        let block = try container.decode(String.self, forKey: .block)
        let lot = try container.decode(String.self, forKey: .lot)
        let fullAddress = try container.decode(String.self, forKey: .fullAddress)
        let neighborhood = try container.decode(String.self, forKey: .neighborhood)
        let lattitude = try container.decode(Float.self, forKey: .lattitude)
        let longitude = try container.decode(Float.self, forKey: .longitude)
        let comps = try container.decode([Comp].self, forKey: .comps)
        
        self.init (zone:zone, accrisURL:accrisURL, dobURL:dobURL, marketValue:marketValue, owner:owner,yearBuilt:yearBuilt, units:units, lotFront:lotFront, lotDepth:lotDepth, landArea:landArea, buidlingClass:buidlingClass, buildingCount:buildingCount, stories:stories, buildingDimensionFront:buildingFront, buildingDimensionDepth:buildingDepth, buildingSQFT:buildingSQFT, boro:boro, block:block, lot:lot, fullAddress:fullAddress,neighborhood:neighborhood, lattitude:Double(lattitude), longitude:Double(longitude),comps:comps)
    }
    
   

}

struct PropertyDescriptor {
    let descriptor : (labelName: String, value: String, category: String, icon: String)
}


