//
//  fetchPropertyDetails.swift
//  Snapshot
//
//  Created by clyon jackson on 11/19/19.
//  Copyright © 2019 Ours Technologies. All rights reserved.
//

import Foundation
import UIKit
import Alamofire


func getPropertyCharacteristics(address: String,
                                resultsViewController: PropertyDetailsViewController,
                                searchBar: SearchBarResultsViewController?,
                                mainVC: MainTableView,
                                completion: @escaping(_ results: PropertyCharacteristics) -> PropertyCharacteristics) {
    
    let parameters: Parameters = ["address":address]
    let endpoint = "https://mqrj7gdv87.execute-api.us-east-1.amazonaws.com/dev/PropertyDetails"
    let request = AF.request(endpoint, method: .get, parameters:parameters)
    
    request.responseJSON { response in
        guard let data = response.data else{return}
        do {
            let propertyObject = completion(try JSONDecoder().decode(PropertyCharacteristics.self, from: data))
            
            // Cache Results
            cache.setObject(propertyObject, forKey: propertyObject.fullAddress as NSString)
        } catch {

            if (error.localizedDescription == "The data couldn’t be read because it is missing.") {
                presentAlertViewController(alertString: "Invalid Address", VC: mainVC)
                mainVC.activityIndicator.stopAnimating()
                mainVC.activityIndicator.removeFromSuperview()
                searchBar?.activityIndicator.stopAnimating()
                searchBar?.activityIndicator.removeFromSuperview()
            } else {
                presentAlertViewController(alertString: "Invalid Address", VC:mainVC)
                mainVC.activityIndicator.stopAnimating()
                searchBar?.activityIndicator.stopAnimating()
            }
        }
    }
}
